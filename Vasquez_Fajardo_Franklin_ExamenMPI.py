# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 09:45:41 2020

@author: Joel Vasquez
"""
import time

import random

import functools

from mpi4py import MPI 

def normalize_vector_sequential(ar):

    result = []

    squared_sum = 0

    for n in ar:

        squared_sum += n * n

    raiz = squared_sum**(.5)

   

    for n in ar:

        result.append(n/raiz)

    return result

 

# Complete the normalize_vector_parallel function below.

def normalize_vector_parallel(ar):

    #implement you solution
    
    ar_count=4000000
    
    matResultado= []
    start= time.time()
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    
    if rank ==0:
        ar=  ar = [random.randrange(1,30) for i in range(ar_count)]
        averow=int(ar_count/(size))
        var=ar_count%(size)
        offset=0
        for dest in range(1,size):
            rows=averow +1 if dest <= var else averow
            comm.send(offset,dest)
            comm.send(rows,dest)
            comm.send(ar,dest)
            
            offset +=rows
            
            for i in range(1,size):
                result=comm.recv(source=i)
                matResultado.appende(result)
                
    if rank > 0:
          offset=comm.recv(source=0) 
          row=comm.recv(source=0) 
          var1=comm.recv(source=0)
          
          squared_sum=0
          for k in range (var1):
             squared_sum  =+ k * k
             
          square = squared_sum **(.5)
          comm.send(square,0)
          end = time.time()
         
          print("Tiempo transcurrido del Proceso Mpi:", end - start)
             
            
    
    

    

 

if __name__ == '__main__':

   

    # Prepare data

    ar_count = 4000000

    #Generate ar_count random numbers between 1 and 30

    ar = [random.randrange(1,30) for i in range(ar_count)]

   

    inicioSec = time.time()

    resultsSec = []

    resultsSec = normalize_vector_sequential(ar)

    finSec =  time.time()

   

    # You can modify this to adapt to your code

    inicioPar = time.time()

    resultsPar = []

    resultsPar = normalize_vector_parallel(ar)

    finPar =  time.time()


   

    print('Results are correct!\n' if functools.reduce(lambda x, y : x and y, map(lambda p, q: p == q,resultsSec,resultsPar), True) else 'Results are incorrect!\n')

    print('Sequential Process took %.3f ms \n' % ((finSec - inicioSec)*1000))
    print('Paralelo Process took %.3f ms \n' % ((inicioPar - finPar)*1000))

    
